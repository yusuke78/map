/*map関連のロジックをまとめます。*/



/*表示範囲内のピンの画像リスト*/
var actPins=[
	"../../img/Result_list/Result_list_map_01_act.png",
	"../../img/Result_list/Result_list_map_02_act.png",
	"../../img/Result_list/Result_list_map_03_act.png",
	"../../img/Result_list/Result_list_map_04_act.png",
	"../../img/Result_list/Result_list_map_05_act.png",
	"../../img/Result_list/Result_list_map_06_act.png",
	"../../img/Result_list/Result_list_map_07_act.png",
	"../../img/Result_list/Result_list_map_08_act.png",
	"../../img/Result_list/Result_list_map_09_act.png"
];

/*表示範囲外のピンの画像リスト*/
var nmlPins=[
	"../../img/Result_list/Result_list_map_01_nml.png",
	"../../img/Result_list/Result_list_map_02_nml.png",
	"../../img/Result_list/Result_list_map_03_nml.png",
	"../../img/Result_list/Result_list_map_04_nml.png",
	"../../img/Result_list/Result_list_map_05_nml.png",
	"../../img/Result_list/Result_list_map_06_nml.png",
	"../../img/Result_list/Result_list_map_07_nml.png",
	"../../img/Result_list/Result_list_map_08_nml.png",
	"../../img/Result_list/Result_list_map_09_nml.png"
];



function mapinit(lat,lon,el){
	var currentpos = {lat:lat,lng:lon};
	var mapoption = {center:currentpos,
			zoom:12};
	var map = new google.maps.Map(document.getElementById("mapid"),mapoption);			
	return map;
}
/*
createMarker
1.指定された場所にマーカを表示します。
引数
lat:ピンを立てる座標(lat)
lon:ピンを立てる座標(lon)
map:initMapで生成したオブジェクト
flg:0→現在地 1→表示範囲のピン 2→表示範囲外のピン
index:何番目のピンか。(現在地はany) 
使用変数
layer:数字が大きいほど、重なり優先度高
優先度　現在地＜表示中＜近い順
y = -x + 8 の比例式で定義
*/
function createMarker(lat,lon,map,flg,index){
	var pin ="";
	var layer = index * -1 + 8;
	switch (flg){
		case 0:
			pin = "../../img/Result_list/Result_list_my_place.png";
			layer = 20;
			break;
		case 1:
			pin = actPins[index];
			break;
		case 2:
			pin = nmlPins[index];
			break;

	}
	lat = lat -0;
	lon = lon -0;
	var pos = {lat:lat,lng:lon};
	var marker = new google.maps.Marker({
			position: pos,
			map: map,
			zIndex:layer, 
			icon: {url: pin,
			       scaledSize: new google.maps.Size(40, 41),
			       origin : new google.maps.Point(0, 0)}		   	
  		});
	return marker;
}

/*座標の範囲を求めます。*/
function getMaxMin(geo){
	
	console.dir(geo);
	var minX=90;	//latの最大値
	var maxX=-90;	//latの最小値
	var minY=180;	//lonの最大値
	var maxY=-180;	//lonの最小値
	//ループさせながら最大値と最小値を探す
	for(var i=0;i<geo.length; i++){
		/*数字に変換*/
		geo[i][0] = geo[i][0]-0;
		geo[i][1] = geo[i][1]-0;
		//latの最小値を探す
		if(geo[i][0] < minX){
			minX = geo[i][0];
		}
		//latの最大値を探す
		if(geo[i][0] > maxX){
			maxX = geo[i][0];

		}
		
		//lonの最小値を探す
		if(geo[i][1] < minY){
			minY = geo[i][1];
		}
		
		//lonの最大値を探す
		if(geo[i][1] > maxY){
			maxY = geo[i][1];
		}

	
	}

	return {maxlat:maxX,
		maxlon:maxY,
		minlat:minX,
		minlon:minY};

}
/*getMaxMinで生成したオブジェクトを使用すること*/
function zoomauto(points,map){
	//北西端の座標を設定
	var sw = new google.maps.LatLng(points.maxlat,points.minlon);
	//東南端の座標を設定
	var ne = new google.maps.LatLng(points.minlat,points.maxlon);
 
	//範囲を設定
	var bounds = new google.maps.LatLngBounds(sw, ne);
 
	//自動調整
	map.fitBounds(bounds,5);

}

function chgPin(marker,flg,index){
	var layer = index * -1 + 9;	
	var pin;
	if(flg){ 
		pin = actPins[index];
		layer = layer + 10;
	}else{
		pin =nmlPins[index];
	}
	var icon= {url: pin,
		   scaledSize: new google.maps.Size(40, 41)}
	var res=marker.setZIndex(layer);
	marker.setIcon(icon);
	console.dir(res);
}

/*ピタゴラスの定理で、距離を求めます*/
/* a^2 + b ^2 = c^2*/
function distance(lat1,lon1,lat2,lon2){
　　var a, b, d;

　　a = lat1 - lat2;
　　b = lon1 - lon2;
　　d = Math.sqrt(Math.pow(a,2) + Math.pow(b,2));
　　return Math.floor(d*100000);

}

function substr(text, len, truncation) {
  if (truncation === undefined) { truncation = ''; }
  var text_array = text.split('');
  var count = 0;
  var str = '';
  for (i = 0; i < text_array.length; i++) {
    var n = escape(text_array[i]);
    if (n.length < 4) count++;
    else count += 2;
    if (count > len) {
      return str + truncation;
    }
    str += text.charAt(i);
  }
  return text;
}

